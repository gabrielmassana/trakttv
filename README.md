[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=580d02cd073ecc0100d5732a&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/580d02cd073ecc0100d5732a/build/latest)

# TraktTV

[TracktTV API](http://docs.trakt.apiary.io/)

[TMDB API](https://developers.themoviedb.org/3/getting-started)

# How and why

### Technical Decisions

 - Xcode 7.2
 - Language: Swift 2.X
 - iOS 9.3
 - iPhone 5/5s/6/6+/6s/6s+
 - Core Data to persist data
 - NSURLSession through Alamofire
 - NSOperaionQueue for background operations
 
### Unit Test

I added some unit tests to the project. I know they are a really small part of the tests that can be added to this project, but they are only trying to be a small example of how powerful they are.

### Continuous Integration

The project is integrated with [BuddyBuild](https://dashboard.buddybuild.com/apps/580d02cd073ecc0100d5732a/build/latest) as Continuous Integration to automate the build and test of the project.

### My own Pods

I used one of [my own pods in Cocoapods](https://cocoapods.org/owners/10374).   
   
- **[ColorWithHex](https://cocoapods.org/pods/ColorWithHex)**. Swift Extension to convert hexadecimal values into `UIColor` Objects.

### Third party Pods

- **[PureLayout](https://cocoapods.org/pods/PureLayout)**. An easy and powerfull pod that helps a lot using auto-layout.
- **[Alamofire](https://cocoapods.org/?q=Alamofire)**. An HTTP networking library written in Swift
- **[CoreDataServices](https://cocoapods.org/pods/CoreDataServices)**. A suite of helper classes and categories to help to remove some of the boilerplate that surrounds using Core Data.
- **[ConvenientFileManager](https://cocoapods.org/pods/ConvenientFileManager)**. A suite of categories to ease using NSFileManager for common tasks. 



## The app meets all the requirements

#### Requirements

1. When the app is launched, a list of the 10 most popular movies is displayed.2. As soon as the user scrolls down and reaches the end of the list, the next 10 movies are loaded.3. The other functionality is the search: the user should have the possibility to search for a movie by keyword. It is important that the search is automatically triggered, i.e. the search is automatically executed while typing the search term. When the user continues typing, the old search should be cancelled and a search for the new term must be started .4. The search results should be scrollable via pagination as well.5. Important properties to show in the search results are the movie title (t itle) , the year of release ( year ), the overview ( overview ) without truncation and a picture .

#### Observations

1. The Movies API payload do not return an overview of the movie, only year and title.
2. [TraktTV Image API](https://apiblog.trakt.tv/trakt-api-images-56b43c356427#.kos8l042f) will be deprecated on October 31, 2016. So I decided to use [TMDB Image API](https://developers.themoviedb.org/3/getting-started/images) service.
