//
//  NavigationItemTitleView.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class NavigationItemTitleView: UILabel {

    //MARK: - Init
    
    init(labelText: String) {
        
        let labelFont = UIFont.tradeGothicLTWithSize(19.0)
        
        let boundingRectSize = CGSize(width: 220.0 * DeviceManager.sharedInstance.resizeFactor,
                                      height: 40.0 * DeviceManager.sharedInstance.resizeFactor)
        
        let titleTextSize = labelText.sizeForText(labelFont,
                                                  boundingRectSize: boundingRectSize)
        
        let frame = CGRect(x: 0.0,
                           y: 0.0,
                           width: titleTextSize.width,
                           height: titleTextSize.height)
        
        super.init(frame: frame)
        
        textAlignment = .Center
        textColor = UIColor.scorpion()
        font = labelFont
        lineBreakMode = NSLineBreakMode.ByTruncatingMiddle
        text = labelText
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
}
