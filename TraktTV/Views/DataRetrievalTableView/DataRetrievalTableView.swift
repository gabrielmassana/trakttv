//
//  DataRetrievalTableView.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

let PaginationOffset = 5

protocol DataRetrievalDelegate: class {
    
    func paginate()
}

class DataRetrievalTableView: UITableView {
    
    //MARK: - Accessors
    
    /// Delegate object
    weak var dataRetrievalDelegate: DataRetrievalDelegate?
    
    var isPaginating = false
    
    //MARK: - UITableView

    override func dequeueReusableCellWithIdentifier(identifier: String, forIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = super.dequeueReusableCellWithIdentifier(identifier,
                                                           forIndexPath: indexPath)
        
        if isPaginating == false {
            
            let rowsInSection = numberOfRowsInSection(indexPath.section)
            let paginationTriggerIndex = rowsInSection - PaginationOffset
            
            let triggerPagination = indexPath.row >= paginationTriggerIndex
            
            if triggerPagination == true {
                
                self.dataRetrievalDelegate?.paginate()
            }
        }
        
        return cell
    }
    
    //MARK: - Paginate
    
    func willPaginate() {
        
        isPaginating = true
    }

    func didPaginate() {
        
        isPaginating = false
    }
}