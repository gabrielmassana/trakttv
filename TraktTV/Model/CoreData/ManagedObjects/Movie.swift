//
//  Movie.swift
//  
//
//  Created by GabrielMassana on 20/10/2016.
//
//

import Foundation
import CoreData


class Movie: NSManagedObject {

    class func fetchMovie(traktID: String, managedObjectContext: NSManagedObjectContext) -> Movie? {
        
        let predicate = NSPredicate(format: "traktID == %@", traktID)
        
        let movie = managedObjectContext.retrieveFirstEntry(Movie.self,
                                                            predicate: predicate) as? Movie
        
        return movie
    }
}
