//
//  Movie+CoreDataProperties.swift
//  
//
//  Created by GabrielMassana on 23/10/2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Movie {

    @NSManaged var creationDate: NSDate?
    @NSManaged var title: String?
    @NSManaged var tmdbID: String?
    @NSManaged var tmdbImagePath: String?
    @NSManaged var traktID: String?
    @NSManaged var year: String?
    @NSManaged var feeds: NSSet?

    @NSManaged func addFeedsObject(dogBreed: MoviesFeed)
    @NSManaged func removeFeedsObject(dogBreed: MoviesFeed)
    @NSManaged func addFeeds(dogBreeds: NSSet)
    @NSManaged func removeFeeds(dogBreeds: NSSet)
}
