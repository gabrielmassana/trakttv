//
//  MoviesFeed.swift
//  
//
//  Created by GabrielMassana on 20/10/2016.
//
//

import Foundation

import CoreData
import CoreDataServices

let MoviesFeedID: String = "-1"

class MoviesFeed: NSManagedObject {

    //MARK - Fetch
    
    class func fetchOrInsertMoviesFeed(feedID: String, managedObjectContext: NSManagedObjectContext) -> MoviesFeed? {
        
        var feed = fetchMoviesFeed(feedID,
                                   managedObjectContext: managedObjectContext)
        
        if let fetchedFeed = feed {
            
            return fetchedFeed
        }
        else {
            
            feed = NSEntityDescription.insertNewObjectForEntity(MoviesFeed.self,
                                                                managedObjectContext: managedObjectContext) as? MoviesFeed
            
            feed?.feedID = feedID
            
            ServiceManager.sharedInstance.saveBackgroundManagedObjectContext()
            
            return feed
        }
    }
    
    private class func fetchMoviesFeed(feedID: String, managedObjectContext: NSManagedObjectContext) -> MoviesFeed? {
        
        let predicate = NSPredicate(format: "feedID == %@", feedID)
        
        let feed = managedObjectContext.retrieveFirstEntry(MoviesFeed.self,
                                                           predicate: predicate) as? MoviesFeed
        
        return feed
    }
    
    //MARK - MoreContent

    func hasMoreContent() -> Bool {
        
        var hasMoreContent = false
        
        if totalPages?.intValue > currentPage?.intValue {
            
            hasMoreContent = true
        }
        
        return hasMoreContent
    }
    
    func hasNoContent() -> Bool {
        
        return currentPage?.intValue == 0
    }
}
