//
//  MoviesFeed+CoreDataProperties.swift
//  
//
//  Created by GabrielMassana on 23/10/2016.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension MoviesFeed {

    @NSManaged var currentPage: NSNumber?
    @NSManaged var feedID: String?
    @NSManaged var totalItems: NSNumber?
    @NSManaged var totalPages: NSNumber?
    @NSManaged var movies: NSSet?

    @NSManaged func addMoviesObject(dogBreed: Movie)
    @NSManaged func removeMoviesObject(dogBreed: Movie)
    @NSManaged func addMovies(dogBreeds: NSSet)
    @NSManaged func removeMovies(dogBreeds: NSSet)
}
