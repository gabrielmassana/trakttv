//
//  UIFont+TraktTV.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

/**
 Accessors for all the fonts used.
 */
extension UIFont {
    
    class func tradeGothicLightWithSize(size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothic-Light",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func tradeGothicLTWithSize(size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothicLT",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
    
    class func tradeGothicNo2BoldWithSize(size: CGFloat) -> UIFont {
        
        return UIFont(name: "TradeGothicNo.2-Bold",
                      size: size * DeviceManager.sharedInstance.resizeFactor)!
    }
}
