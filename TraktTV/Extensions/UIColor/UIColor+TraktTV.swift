//
//  UIColor+TraktTV.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//
// Naming: http://chir.ag/projects/name-that-color/
//

import UIKit

import ColorWithHex


// Accessors for all the colors used.
extension UIColor {
    
    class func scorpion() -> UIColor {
        
        return UIColor.colorWithHex("606060")!
    }
    
    class func alto() -> UIColor {
        
        return UIColor.colorWithHex("DFDFDF")!
    }
}
