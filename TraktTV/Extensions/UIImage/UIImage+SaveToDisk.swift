//
//  UIImage+SaveToDisk.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

extension UIImage {
    
    func saveToDisk(imageID: String) {
        
        if let data = UIImageJPEGRepresentation(self, 0.5) {
            
            // Save image to Disk
            NSFileManager.saveDataToCacheDirectory(data,
                                                   relativePath: imageID)
        }
    }
}
