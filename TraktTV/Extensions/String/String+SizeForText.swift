//
//  String+SizeForText.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

/// Helps calculate the size of a string.
extension String {
    
    /**
     Calculate the size of a string depending on its font, text and container size.
     
     - Parameter font: The font of the string text.
     - Parameter boundingRectSize: The size of the rectangle where the text should fit.
     
     - Returns: The size of the text.
     */
    func sizeForText(font: UIFont, boundingRectSize: CGSize) -> CGSize {
        
        let textRect = self.boundingRectWithSize(boundingRectSize,
                                                 options: [
                                                    NSStringDrawingOptions.UsesLineFragmentOrigin,
                                                    NSStringDrawingOptions.UsesFontLeading],
                                                 attributes: [NSFontAttributeName : font],
                                                 context: nil)
        
        return textRect.size
    }
}
