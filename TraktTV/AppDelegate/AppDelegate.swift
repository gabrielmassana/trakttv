//
//  AppDelegate.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreDataServices

/// Operation Queue identifier for Local Data Operations.
let LocalDataOperationQueueTypeIdentifier: String = "LocalDataOperationQueueTypeIdentifier"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK: - Accessors
    
    /// App Window
    var window: UIWindow? = {
        
        let window: UIWindow = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        window.backgroundColor = UIColor.whiteColor()
        
        return window
    }()
    
    /// The Navigation Controller working as rootViewController.
    lazy var rootNavigationController: RootNavigationController = {
        
        var rootNavigationController = RootNavigationController()
        
        return rootNavigationController
    }()
    
    //MARK: - UIApplicationDelegate

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        ServiceManager.sharedInstance.setupModel("TraktTV")
        
        /*-------------------*/
        
        registerOperationQueues()
        
        /*-------------------*/
        
        window!.rootViewController = rootNavigationController
        window!.makeKeyAndVisible()
        
        return true
    }
    
    //MARK: - OperationQueues
    
    /**
     Registers the operations queues in the app.
     */
    func registerOperationQueues() {
        
        // e.g.: Parse and Core Data updates
        let localDataOperationQueue: NSOperationQueue = NSOperationQueue()
        localDataOperationQueue.qualityOfService = .UserInteractive
        localDataOperationQueue.maxConcurrentOperationCount = NSOperationQueueDefaultMaxConcurrentOperationCount
        OperationQueueManager.sharedInstance.register(operationQueue: localDataOperationQueue,
                                                      operationQueueIdentifier: LocalDataOperationQueueTypeIdentifier)
    }
}

