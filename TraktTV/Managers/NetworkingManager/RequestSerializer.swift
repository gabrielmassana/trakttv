//
//  RequestSerializer.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Alamofire

extension Alamofire.Request {
    
    /// Handle the API response and parse the errors on it.
    func responseJSONTraktTV(
        queue queue: dispatch_queue_t? = nil,
              options: NSJSONReadingOptions = .AllowFragments,
              completionHandler: (Response<AnyObject, NSError>, NSError) -> Void)
        -> Self {
            
            return self.responseJSON { (response) in
                
                let error = ErrorSerializer.serializeError(response.result.error,
                    data: response.data)
                
                completionHandler(response, error)
            }
    }
}