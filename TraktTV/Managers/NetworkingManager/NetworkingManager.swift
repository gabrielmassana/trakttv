//
//  NetworkingManager.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import Alamofire

/// Manager to handle networking based on Alamofire.
class NetworkingManager: NSObject {

    //MARK: - Accessors
    
    /// Networking manager with Default Session Configuration
    static var defaultManager: Alamofire.Manager = {
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Manager.defaultHTTPHeaders
        
        let defaultManager = Alamofire.Manager(configuration: configuration)
        
        return defaultManager
    }()
    
    //MARK: - Init
    
    override init() {
        
        super.init()
    }
}
