//
//  ErrorSerializer.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

public struct TraktTVError {
    
    /// The domain used for creating all TraktTV errors.
    public static let Domain = "com.trakttv.error"
}

enum TraktTVErrorCode: Int {
    
    case ConnectionTraktTVError = -1007
    case UnknownTraktTVError = -1
    case KnownTraktTVError = -42200
    case MessageTraktTVError = -40400
}

class ErrorSerializer: NSObject {
    
    static let unknownError: NSError = {
        
        return NSError(domain: TraktTVError.Domain,
                       code: TraktTVErrorCode.UnknownTraktTVError.rawValue,
                       userInfo: [
                        NSLocalizedDescriptionKey : "Unknown error"
            ])
    }()
    
    class func serializeError(error: NSError?, data: NSData?) -> NSError {
        
        //TODO
        
        return unknownError
    }
}