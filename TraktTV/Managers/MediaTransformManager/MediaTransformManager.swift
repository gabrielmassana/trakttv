//
//  MediaTransformManager.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class MediaTransformManager: NSObject {

    //MARK: - TranformedMedia
    
    class func tranformedMedia(image: UIImage) -> UIImage {
        
        // Crop Image to Aspect ratio
        let cropRect = calculateCropRect(image)
        
        let croppedImage = cropImage(image,
                                     cropRect: cropRect)
        
        // Resize image to UI Size
        let resizedImage = resizeImage(croppedImage)
        
        return resizedImage
    }
    
    // MARK - Crop
    
    private class func cropImage(image: UIImage, cropRect: CGRect) -> UIImage {
        
        guard let imageRef:CGImageRef = CGImageCreateWithImageInRect(image.CGImage, cropRect) else {
            
            return image
        }
        
        let croppedImage:UIImage = UIImage(CGImage:imageRef)
        
        return croppedImage
    }
    
    private class func calculateCropRect(image: UIImage) -> CGRect {
        
        // Calculate projectImageRatio
        let projectImageRatio: CGFloat = MovieImageSize.Width / MovieImageSize.Height
        
        // Calculate cropRect
        var cropRect = CGRectZero
        let imageRatio = image.size.width / image.size.height
        
        if imageRatio < projectImageRatio {
            
            let width = image.size.width
            let height = image.size.width / projectImageRatio
            let x: CGFloat = 0.0
            let y = (image.size.height - height) / 2.0
            
            cropRect = CGRect(x: x,
                              y: y,
                              width: width,
                              height: height)
        }
        else {
            
            let width = image.size.height * projectImageRatio
            let height = image.size.height
            let x = (image.size.width - width) / 2.0
            let y: CGFloat = 0.0
            
            cropRect = CGRect(x: x,
                              y: y,
                              width: width,
                              height: height)
        }
        
        return cropRect
    }
    
    //MARK: - ResizeImage
    
    private class func resizeImage(image: UIImage) -> UIImage {
        
        // Calculate drawRect
        
        let drawRect = CGRectMake(0.0,
                                  0.0,
                                  MovieImageSize.Width,
                                  MovieImageSize.Height)
            
        
        // Resize Image
        UIGraphicsBeginImageContextWithOptions(drawRect.size,
                                               false,
                                               UIScreen.mainScreen().scale)
        
        image.drawInRect(drawRect)
        
        let resizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
}
