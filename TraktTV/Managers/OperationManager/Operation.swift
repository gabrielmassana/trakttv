//
//  Operation.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

/// Success clousure.
typealias OperationOnSuccess = @convention(block) (result: AnyObject?) -> Void

/// Failure clousure.
typealias OperationOnFailure = @convention(block) (error: NSError?) -> Void

/// Completion clousure.
typealias OperationOnCompletion = @convention(block) (result: AnyObject?) -> Void

/// Subclass of NSOperation with callbacks and auto coalesce.
class Operation: NSOperation {
    
    //MARK: - Accessors
    
    /// Unique identifier used for coalescing, operations with the same identifier will be coalesced in the operation coordinator.
    var identifier: String?
    
    /// This is the identifier of the operation queue you would like the operation to be run on.
    var operationQueueIdentifier: String?
    
    /// Callback called when the operation pletes succesfully.
    var onSuccess: OperationOnSuccess?
    
    /// Callback called when the operation fails.
    var onFailure: OperationOnFailure?
    
    /// Callback called when the operation completes. The completion block is used instead of the success/failure blocks not alongside.
    var onCompletion: OperationOnCompletion?
    
    /// The result of the operation.
    var result: AnyObject?
    
    /// The error of the operation.
    var error: NSError?
    
    /// Current Operation Queue.
    var callbackQueue: NSOperationQueue?
    
    private var _ready: Bool = true
    private var _executing: Bool = false
    private var _finished: Bool = false
    private var _identifier: String?
    
    //MARK: - Init
    
    override init() {
        
        super.init()
        
        self.ready = true
        callbackQueue = NSOperationQueue.currentQueue()
    }
    
    //MARK: - Name
    
    override var name: String? {
        
        get {
            
            return identifier
        }
        set {
            
        }
    }
    
    //MARK: - State
    
    override var ready: Bool {
        
        get {
            
            return _ready
        }
        set {
            
            if _ready != newValue {
                
                willChangeValueForKey("isReady")
                _ready = newValue
                didChangeValueForKey("isReady")
            }
        }
    }
    
    override var executing: Bool {
        
        get {
            
            return _executing
        }
        set {
            
            if _executing != newValue {
                
                willChangeValueForKey("isExecuting")
                _executing = newValue
                didChangeValueForKey("isExecuting")
            }
        }
    }
    
    override var finished: Bool {
        
        get {
            
            return _finished
        }
        set {
            
            if _finished != newValue {
                
                willChangeValueForKey("isFinished")
                _finished = newValue
                didChangeValueForKey("isFinished")
            }
        }
    }
    
    override var asynchronous: Bool {
        
        get {
            
            return true
        }
        set {
            
        }
    }
    
    //MARK: - Control
    
    override func start() {
        
        if !executing {
            
            ready = false
            executing = true
            finished = false
            
            if let name = self.name {
                
                print("\(name) - Operation will start.")
            }
            else {
                
                print("\(self.name) - Operation will start.")
            }
        }
    }
    
    /**
     Finishes the execution of the operation.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func finish() {
        
        if executing {
            
            if let name = self.name {
                
                print("\(name) - Operation did finish.")
            }
            else {
                
                print("\(self.name) - Operation did finish.")
            }
            
            executing = false
            finished = true
        }
    }
    
    //MARK: - Callbacks
    
    /**
     Finishes the execution of the operation and calls the onSuccess callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didSucceedWithResult(result: AnyObject?) {
        
        self.result = result
        
        finish()
        
        if let onSuccess = onSuccess {
            
            callbackQueue?.addOperationWithBlock({
                
                onSuccess(result: result)
            })
        }
    }
    
    /**
     Finishes the execution of the operation and calls the onFailure callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didFailWithError(error: NSError?) {
        
        self.error = error
        
        finish()
        
        if let onFailure = onFailure {
            
            callbackQueue?.addOperationWithBlock({
                
                onFailure(error: error)
            })
        }
    }
    
    /**
     Finishes the execution of the operation and calls the onCompletion callback.
     
     - Note: This shouldn't be called externally as this is used internally by subclasses.
     To cancel an operation use cancel instead.
     */
    func didCompleteWithResult(result: AnyObject?) {
        
        self.result = result
        
        finish()
        
        if let onCompletion = onCompletion {
            
            callbackQueue?.addOperationWithBlock({
                
                onCompletion(result: result)
            })
        }
    }
    
    //MARK: - Coalescing
    
    /**
     This method figures out if we can coalesce with another operation.
     
     - Parameter operation: Operation to determaine if we can coalesce with.
     - Returns: YES if we can coaslesce with it. NO otherwise.
     */
    func canCoalesceWithOperation(operation: Operation) -> Bool {
        
        return identifier == operation.identifier
    }
    
    /**
     This method coalesces another operation with this one.
     
     - Parameter operation: Operation to coalesce with.
     */
    func coalesceWithOperation(operation: Operation) {
        
        // Success coalescing
        let mySuccessBlock = onSuccess
        let theirSuccessBlock = operation.onSuccess
        
        if mySuccessBlock != nil ||
            theirSuccessBlock != nil {
            
            onSuccess = { (result: AnyObject?) in
                
                mySuccessBlock?(result: result)
                theirSuccessBlock?(result: result)
            }
        }
        
        // Failure coalescing
        let myFailureBlock = onFailure
        let theirFailureBlock = operation.onFailure
        
        if myFailureBlock != nil ||
            theirFailureBlock != nil {
            
            onFailure = { (error: NSError?) in
                
                myFailureBlock?(error: error)
                theirFailureBlock?(error: error)
            }
        }
        
        // Completion coalescing
        let myCompletionBlock = onCompletion
        let theirCompletionBlock = operation.onCompletion
        
        if myCompletionBlock != nil ||
            theirCompletionBlock != nil {
            
            onCompletion = { (result: AnyObject?) in
                
                myCompletionBlock?(result: result)
                theirCompletionBlock?(result: result)
            }
        }
    }
}
