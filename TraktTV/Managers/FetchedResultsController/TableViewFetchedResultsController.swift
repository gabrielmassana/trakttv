//
//  TableViewFetchedResultsController.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreData

protocol TableViewFetchedResultsControllerDelegate: class {
    
    /**
     Informational call for when the FRC updates.
     */
    func didUpdateContent()
    
    /**
     Informational call for when the FRC updates.
     
     - param indexPath: indexPath to the updated cell.
     */
    func didUpdateCell(indexPath: NSIndexPath)
}

class TableViewFetchedResultsController: NSFetchedResultsController {
    
    //MARK: Accessors
    
    /// TableViewFetchedResultsControllerDelegate delegate object
    weak var frcDelegate: TableViewFetchedResultsControllerDelegate?
    
    /// Table view for the fetch result controller to update.
    weak var tableView: UITableView?
    
    /// Specifies if the fetch result controller should update it's sections.
    var shouldUpdateSections: Bool = true
    
    /// Disables all animations when updating table view.
    var disableAnimations = false
    
    /**
     Animation effect on an insert row action.
     */
    var insertRowAnimation: UITableViewRowAnimation = UITableViewRowAnimation.Automatic
    
    /**
     Animation effect on a delete row action.
     */
    var deleteRowAnimation: UITableViewRowAnimation = UITableViewRowAnimation.Automatic
    
    /**
     Animation effect on an insert section action.
     */
    var insertSectionAnimation: UITableViewRowAnimation = UITableViewRowAnimation.Automatic
    
    /**
     Animation effect on a delete section action.
     */
    var deleteSectionAnimation: UITableViewRowAnimation = UITableViewRowAnimation.Automatic
    
    //MARK: Init
    
    override init(fetchRequest: NSFetchRequest, managedObjectContext context: NSManagedObjectContext, sectionNameKeyPath: String?, cacheName name: String?) {
        
        super.init(fetchRequest: fetchRequest,
                   managedObjectContext: context,
                   sectionNameKeyPath: sectionNameKeyPath,
                   cacheName: name)
        
        self.delegate = self
    }
}

extension TableViewFetchedResultsController: NSFetchedResultsControllerDelegate {
    
    //MARK: NSFetchedResultsControllerDelegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        
        self.tableView?.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController,
                    didChangeObject anObject: AnyObject,
                                    atIndexPath indexPath: NSIndexPath?,
                                                forChangeType type: NSFetchedResultsChangeType,
                                                              newIndexPath: NSIndexPath?) {
        
        switch type {
            
        case NSFetchedResultsChangeType.Insert:
            
            if let newIndexPath = newIndexPath {
                
                self.tableView?.insertRowsAtIndexPaths([newIndexPath],
                                                       withRowAnimation: self.insertRowAnimation)
            }
            
        case NSFetchedResultsChangeType.Delete:
            
            if let indexPath = indexPath {
                
                self.tableView?.deleteRowsAtIndexPaths([indexPath],
                                                       withRowAnimation: self.deleteRowAnimation)
            }
            
        case NSFetchedResultsChangeType.Update:
            
            if let indexPath = indexPath {
                
                self.frcDelegate?.didUpdateCell(indexPath)
            }
            
        case NSFetchedResultsChangeType.Move:
            
            if let newIndexPath = newIndexPath,
                let indexPath = indexPath {
                
                self.tableView?.deleteRowsAtIndexPaths([indexPath],
                                                       withRowAnimation: self.deleteRowAnimation)
                
                self.tableView?.insertRowsAtIndexPaths([newIndexPath],
                                                       withRowAnimation: self.insertRowAnimation)
            }
        }
    }
    
    func controller(controller: NSFetchedResultsController,
                    didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
                                     atIndex sectionIndex: Int,
                                             forChangeType type: NSFetchedResultsChangeType) {
        
        if shouldUpdateSections {
            
            switch type {
            case NSFetchedResultsChangeType.Insert:
                
                self.tableView?.insertSections(NSIndexSet(index: sectionIndex),
                                               withRowAnimation: self.insertSectionAnimation)
                
            case NSFetchedResultsChangeType.Delete:
                
                self.tableView?.deleteSections(NSIndexSet(index: sectionIndex),
                                               withRowAnimation: self.deleteRowAnimation)
                
            case NSFetchedResultsChangeType.Move,
                 NSFetchedResultsChangeType.Update:
                
                ()
            }
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        
        var contentOffset = CGPointZero
        
        if disableAnimations == true {
            
            contentOffset = tableView!.contentOffset
            UIView.setAnimationsEnabled(false)
        }
        
        tableView?.endUpdates()
        
        if disableAnimations == true {
            
            UIView.setAnimationsEnabled(true)
            tableView?.contentOffset = contentOffset
        }
        
        frcDelegate?.didUpdateContent()
    }
}
