//
//  PopularMoviesAdapter.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreDataServices
import CoreData

protocol PopularMoviesAdapterDelegate: class {

}

class PopularMoviesAdapter: NSObject {

    //MARK: - Accessors
    
    /// Delegate object
    weak var delegate: PopularMoviesAdapterDelegate?
    
    var searchCriteria = MoviesFeedID
    
    var heightAtIndexPath = [NSIndexPath : NSNumber]()
    
    /// Table view to display data.
    var tableView: DataRetrievalTableView! {
        
        willSet (newValue) {
            
            if newValue != tableView {
                
                self.tableView = newValue
                
                tableView.dataSource = self
                tableView.delegate = self
                tableView.dataRetrievalDelegate = self
                tableView.backgroundColor = UIColor.whiteColor()
                tableView.separatorStyle = .None
                
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = UITableViewAutomaticDimension
                
                // RegisterCells
                registerCells()
                
                // FRC performFetch
                let _ = try? self.fetchedResultsController.performFetch()
            }
        }
    }
    
    /// Used to connect the TableView with Core Data.
    lazy var fetchedResultsController: TableViewFetchedResultsController =  {
        
        let fetchedResultsController = TableViewFetchedResultsController(fetchRequest: self.fetchRequest,
                                                                         managedObjectContext: ServiceManager.sharedInstance.mainManagedObjectContext,
                                                                         sectionNameKeyPath: nil,
                                                                         cacheName: nil)
        
        fetchedResultsController.tableView = self.tableView
        fetchedResultsController.deleteRowAnimation = UITableViewRowAnimation.None
        fetchedResultsController.frcDelegate = self
        fetchedResultsController.disableAnimations = true
        
        return fetchedResultsController
    }()
    
    /// Fetch request for movies.
    lazy var fetchRequest: NSFetchRequest = {
        
        let fetchRequest = NSFetchRequest()
        
        fetchRequest.entity =  NSEntityDescription.entityFor(Movie.self,
                                                             managedObjectContext: ServiceManager.sharedInstance.mainManagedObjectContext)
        
        fetchRequest.sortDescriptors = self.sortDescriptors
        fetchRequest.predicate = self.predicate
        
        return fetchRequest
    }()
    
    /// Predicate to filter the fetch request
    var predicate: NSPredicate = {
        
        let predicate = NSPredicate(format: "ANY feeds.feedID == %@", MoviesFeedID)
        
        return predicate
    }()
    
    /// Sort Descriptors to sort how movies should be ordered.
    lazy var sortDescriptors: [NSSortDescriptor] = {
        
        let sortDescriptors: NSSortDescriptor = NSSortDescriptor(key: "creationDate",
                                                                 ascending: true)
        
        return [sortDescriptors]
    }()
    
    //MARK: - RegisterCells
    
    /**
     Register the cells to be used in the table view.
     */
    func registerCells() {
        
        tableView.registerClass(PopularMovieCell.self,
                                forCellReuseIdentifier: PopularMovieCell.reuseIdentifier())
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the cell.
     
     - Parameter cell: cell to be configured.
     - Parameter indexPath: cell indexPath.
     */
    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        
        guard let fetchedObjects = fetchedResultsController.fetchedObjects,
            let movie = fetchedObjects[indexPath.row] as? Movie else {
                
                return
        }
        
        let cell = cell as! PopularMovieCell
        cell.configureWithMovie(movie)
        cell.layoutByApplyingConstraints()
        cell.rasterizeLayer()
    }
    
    //MARK: - Search
    
    func searchMoviesWithSearchText(searchCriteria: String) {
        
        // Update searchCriteria.
        self.searchCriteria = searchCriteria
        
        // Cancel running tasks.
        NetworkingManager.defaultManager.session.getAllTasksWithCompletionHandler { tasks in
            
            tasks.forEach {
                
                $0.cancel()
            }
            
            // Search API.
            self.refresh {
                
                // Push to main thread
                dispatch_async(dispatch_get_main_queue(), {
                    
                    // Update TableView.
                    self.updateTableViewWithSearchCriteria(searchCriteria)
                })
            }
        }
    }
    
    //MARK: - UpdateSearch
    
    private func updateTableViewWithSearchCriteria(searchCriteria: String) {
        
        let predicate: NSPredicate = NSPredicate(format: "ANY feeds.feedID == %@", searchCriteria)
        
        fetchedResultsController.fetchRequest.predicate = predicate
        
        let _ = try? self.fetchedResultsController.performFetch()
        tableView.reloadData()
    }
}

extension PopularMoviesAdapter: UITableViewDataSource {
    
    //MARK: - UITableViewDataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let count = fetchedResultsController.fetchedObjects?.count else {
            
            return 0
        }
        
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(PopularMovieCell.reuseIdentifier(),
                                                           forIndexPath: indexPath)

        configureCell(cell,
                      indexPath: indexPath)
        
        return cell
    }
}

extension PopularMoviesAdapter: UITableViewDelegate {
    
    //MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView,
                   estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let height = heightAtIndexPath[indexPath]
        
        if let height = height {
            
            return CGFloat(height)
        }
        else {
            
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(tableView: UITableView,
                   willDisplayCell cell: UITableViewCell,
                                   forRowAtIndexPath indexPath: NSIndexPath) {
        
        let height: NSNumber = CGRectGetHeight(cell.frame)
        heightAtIndexPath[indexPath] = height
    }
    
    func tableView(tableView: UITableView,
                   didEndDisplayingCell cell: UITableViewCell,
                                        forRowAtIndexPath indexPath: NSIndexPath) {
        
        let height: NSNumber = CGRectGetHeight(cell.frame)
        heightAtIndexPath[indexPath] = height
    }
}

extension PopularMoviesAdapter: TableViewFetchedResultsControllerDelegate {
    
    //MARK: - TableViewFetchedResultsControllerDelegate
    
    func didUpdateCell(indexPath: NSIndexPath) {
        
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) else {
            
            return
        }
        
        self.configureCell(cell,
                           indexPath: indexPath)
    }
    
    func didUpdateContent() {
        
    }
}

extension PopularMoviesAdapter: DataRetrievalDelegate {
    
    // Move to Adapter
    func paginate() {
        
        let moviesFeed = MoviesFeed.fetchOrInsertMoviesFeed(searchCriteria,
                                                            managedObjectContext: ServiceManager.sharedInstance.mainManagedObjectContext)
        
        if moviesFeed?.hasMoreContent() == true,
        let currentPage = moviesFeed?.currentPage {
            
            let page = currentPage.integerValue + 1

            tableView.willPaginate()
            
            MoviesAPIManager.retrieveMoviesWith(searchCriteria, page: page, success: { [weak self]  (result) in
                
                if let strongSelf = self {
                    
                    strongSelf.tableView.didPaginate()
                }
                
                })
            { [weak self] (error) in
                
                if let strongSelf = self {
                    
                    strongSelf.tableView.didPaginate()
                }
            }
        }
    }
    
    func refresh(onCompletion:() -> Void) {
        
        let moviesFeed = MoviesFeed.fetchOrInsertMoviesFeed(searchCriteria,
                                                            managedObjectContext: ServiceManager.sharedInstance.mainManagedObjectContext)
        
        if moviesFeed?.hasNoContent() == true {
            
            tableView.willPaginate()
            
            MoviesAPIManager.retrieveMoviesWith(searchCriteria, page: 1, success: { [weak self]  (result) in
                                                    
                if let strongSelf = self {
                    
                    strongSelf.tableView.didPaginate()
                    onCompletion()
                }
                
                })
            { [weak self] (error) in
                
                if let strongSelf = self {
                    
                    strongSelf.tableView.didPaginate()
                }
            }
        }
        else {
            
            onCompletion()
        }
    }
}
