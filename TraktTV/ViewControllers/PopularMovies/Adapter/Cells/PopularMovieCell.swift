//
//  PopularMovieCell.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

struct MovieImageSize {
    
    static var Height: CGFloat = 120.0 * DeviceManager.sharedInstance.resizeFactor
    static var Width: CGFloat = 80.0 * DeviceManager.sharedInstance.resizeFactor
}

class PopularMovieCell: UITableViewCell, TableViewCell {

    //MARK: - Accessors
    
    /// The movie related to the cell.
    var movie: Movie?
    
    lazy var movieImageView: UIImageView = {
       
        var movieImageView = UIImageView.newAutoLayoutView()
        
        movieImageView.backgroundColor = UIColor.scorpion()
        
        return movieImageView
    }()
    
    lazy var movieTitleLabel: UILabel = {
        
        var movieTitleLabel = UILabel.newAutoLayoutView()
        
        movieTitleLabel.font = UIFont.tradeGothicLTWithSize(20.0)
        movieTitleLabel.textAlignment = .Left
        movieTitleLabel.textColor = UIColor.scorpion()
        movieTitleLabel.numberOfLines = 3
        movieTitleLabel.adjustsFontSizeToFitWidth = true
        movieTitleLabel.minimumScaleFactor = 0.5
        
        return movieTitleLabel
    }()
    
    lazy var movieYearLabel: UILabel = {
        
        var movieYearLabel = UILabel.newAutoLayoutView()
        
        movieYearLabel.font = UIFont.tradeGothicLightWithSize(24.0)
        movieYearLabel.textAlignment = .Left
        movieYearLabel.textColor = UIColor.scorpion()
        
        return movieYearLabel
    }()
    
    lazy var separationLine: UIView = {
        
        var separationLine = UIView.newAutoLayoutView()
        
        separationLine.backgroundColor = UIColor.scorpion()
        
        return separationLine
    }()
    
    //MARK: - Init
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .None
        
        contentView.addSubview(movieImageView)
        contentView.addSubview(movieTitleLabel)
        contentView.addSubview(movieYearLabel)
        contentView.addSubview(separationLine)
    }
    
    //MARK: - Constraints
    
    override func updateConstraints() {
        
        movieImageView.autoPinEdgeToSuperviewEdge(.Top,
                                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieImageView.autoPinEdgeToSuperviewEdge(.Bottom,
                                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieImageView.autoPinEdgeToSuperviewEdge(.Left,
                                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        NSLayoutConstraint.autoSetPriority(UILayoutPriorityDefaultHigh) {
            
            movieImageView.autoSetDimension(.Height,
                                            toSize: MovieImageSize.Height)
        }
        
        movieImageView.autoSetDimension(.Width,
                                        toSize: MovieImageSize.Width)
        
        /*-------------------*/
        
        movieTitleLabel.autoPinEdgeToSuperviewEdge(.Top,
                                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieTitleLabel.autoPinEdgeToSuperviewEdge(.Right,
                                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieTitleLabel.autoPinEdge(.Left,
                                    toEdge: .Right,
                                    ofView: movieImageView,
                                    withOffset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        movieYearLabel.autoPinEdge(.Top,
                                   toEdge: .Bottom,
                                   ofView: movieTitleLabel,
                                   withOffset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieYearLabel.autoPinEdgeToSuperviewEdge(.Right,
                                                   withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        movieYearLabel.autoPinEdge(.Left,
                                    toEdge: .Right,
                                    ofView: movieImageView,
                                    withOffset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        /*-------------------*/
        
        separationLine.autoPinEdgeToSuperviewEdge(.Bottom)
        
        separationLine.autoSetDimension(.Height,
                                        toSize: 0.5 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdgeToSuperviewEdge(.Right,
                                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)
        
        separationLine.autoPinEdgeToSuperviewEdge(.Left,
                                                  withInset: 10.0 * DeviceManager.sharedInstance.resizeFactor)

        /*-------------------*/
        
        super.updateConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    //MARK: - ConfigureCell
    
    /**
     Configure the self cell based on the movie data.
     
     - Parameter movie: The movie with the data related to the cell.
     */
    func configureWithMovie(movie: Movie) {
        
        self.movie = movie
        
        movieTitleLabel.text = movie.title
        movieYearLabel.text = movie.year
        
        MediaRetrieval.retrieveMovieImage(movie) { (imageMovie, image) in
            
            if self.movie?.traktID == imageMovie.traktID {
                
                self.movieImageView.image = image
            }
        }
    }
    
    //MARK: - PrepareForReuse
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        movieImageView.image = nil
    }
}
