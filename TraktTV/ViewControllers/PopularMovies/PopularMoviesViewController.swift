//
//  PopularMoviesViewController.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import PureLayout
import CoreDataServices

class PopularMoviesViewController: UIViewController {

    //MARK: - Accessors

    /// Table view to display the movies.
    lazy var tableView: DataRetrievalTableView = {
        
        let tableView: DataRetrievalTableView = DataRetrievalTableView.newAutoLayoutView()
        
        // Important to allow pan down to dismiss the keyboard.
        tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive
        
        return tableView
    }()
    
    /// Adapter to manage the common logic and data of the tableView.
    lazy var adapter: PopularMoviesAdapter = {
        
        let adapter = PopularMoviesAdapter()
        
        adapter.delegate = self
        
        return adapter
    }()
    
    lazy var searchButton: UIBarButtonItem = {
        
        let searchButton = UIBarButtonItem(barButtonSystemItem: .Search,
                                           target: self,
                                           action: #selector(searchButtonPressed(_:)))
        
        searchButton.tintColor = UIColor.scorpion()
        
        return searchButton
    }()
    
    var searchBarSearchButtonClicked = false
    
    lazy var navigationItemTitleView: NavigationItemTitleView = {
    
        let navigationItemTitleView = NavigationItemTitleView(labelText: NSLocalizedString("Popular Movies", comment: "popular_movies_title"))
        
        return navigationItemTitleView
    }()
    
    lazy var searchController : UISearchController = {
        
        let searchController : UISearchController = UISearchController(searchResultsController: nil)
        
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        
        searchController.searchBar.tintColor = UIColor.scorpion()

        return searchController
    }()
    
    //MARK: - ViewLifeCycle

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        view.addSubview(tableView)
        
        adapter.tableView = tableView
        adapter.refresh { }

        /*-------------------*/
        
        updateViewConstraints()
        
        /*-------------------*/

        view.backgroundColor = UIColor.alto()
        
        navigationItem.titleView = navigationItemTitleView
        navigationItem.rightBarButtonItem = searchButton
        
        definesPresentationContext = true
    }
    
    //MARK: - Constraints
    
    override func updateViewConstraints() {
        
        super.updateViewConstraints()
        
        /*-------------------*/
        
        tableView.autoPinEdgesToSuperviewEdges()
    }
    
    //MARK: - ButtonActions

    func searchButtonPressed(sender: UIBarButtonItem) {
        
        navigationItem.titleView = searchController.searchBar
    }
}

extension PopularMoviesViewController : UISearchControllerDelegate {
    
    //MARK: - UISearchControllerDelegate

}

extension PopularMoviesViewController : UISearchResultsUpdating {
    
    //MARK: - UISearchResultsUpdating

    func updateSearchResultsForSearchController(searchController: UISearchController) {
        
        if let searchCriteria = searchController.searchBar.text {
            
            if searchCriteria.characters.count > 0 {
                
                adapter.searchMoviesWithSearchText(searchCriteria)
            }
        }
    }
}

extension PopularMoviesViewController : UISearchBarDelegate {
    
    //MARK: - UISearchBarDelegate

    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        navigationItem.rightBarButtonItem = searchButton
        navigationItem.titleView = navigationItemTitleView
        adapter.searchMoviesWithSearchText(MoviesFeedID)
    }

    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
    
        navigationItem.rightBarButtonItem = nil
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        
        if searchBarSearchButtonClicked == false {
            
            navigationItem.rightBarButtonItem = searchButton
            navigationItem.titleView = navigationItemTitleView
            adapter.searchMoviesWithSearchText(MoviesFeedID)
        }
        
        searchBarSearchButtonClicked = false
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        
        searchBarSearchButtonClicked = true
    }
}

extension PopularMoviesViewController : PopularMoviesAdapterDelegate {
    
    //MARK: - PopularMoviesAdapterDelegate

}