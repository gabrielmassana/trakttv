//
//  APIConfig.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class APIConfig: NSObject {

    //MARK: - Singleton
    
    /// Returns the global APIConfig instance.
    static let sharedInstance = APIConfig()
    
    //MARK: - TraktTV

    /// Version to use for the API.
    let TraktAPIVersion: String = {
        
        return "2"
    }()
    
    /// Base path to the API.
    let TraktAPIHost: String = {
        
        return "https://api.trakt.tv"
    }()
    
    /// The client id.
    let TraktAPIClientID: String = {
        
        return "019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
    }()
    
    //MARK: - TMDB

    /// Base path to the API.
    let TMDBAPIHost: String = {
        
        return "https://api.themoviedb.org"
        
    }()
    
    /// Base path to the Image API.
    let TMDBImageHost: String = {
        
        return "https://image.tmdb.org/t/p"
        
    }()
    
    /// File size Image API.
    let TMDBFileSize: String = {
        
        return "/w342"
        
    }()
    
    /// Version to use for the API.
    let TMDBAPIVersion: String = {
        
        return "3"
    }()
    
    /// The API Key.
    let TMDBAPIKey: String = {
        
        return "258950a2e1e595defad61d85d98502b7"
    }()
    
    /// The API Token.
    let TMDBAPIToken: String = {
        
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyNTg5NTBhMmUxZTU5NWRlZmFkNjFkODVkOTg1MDJiNyIsInN1YiI6IjU4MDlkZTRiYzNhMzY4MTQ3YzAwMTBmYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.-76j8J3cC7WuMPFjQx9ayeOSklcSGgu4gqme4MLIHLg"
    }()
}
