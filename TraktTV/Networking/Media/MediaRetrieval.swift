//
//  MediaRetrieval.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class MediaRetrieval: NSObject {

    //MARK: - Retrieve

    class func retrieveMovieImage(movie: Movie, completion:((movie: Movie, image: UIImage?) -> Void)) {
        
        guard let tmdbID = movie.tmdbID else {
            
            completion(movie: movie,
                       image: nil)
            return
        }
        
        retrieveLocalImage(tmdbID) { (image) in
            
            if let image = image {
                
                completion(movie: movie,
                           image: image)
            }
            else {
                
                if let _ = movie.tmdbImagePath {
                    
                    // download image
                    downloadImage(movie, completion: { (image) in
                        
                        // Transform and save to disk
                        transformAndSave(image, imageID: tmdbID, completion: { (image) in
                            
                            completion(movie: movie,
                                image: image)
                        })
                    })
                }
                else {
                    
                    // download path
                    retrieveTMDBPath(movie, completion: { (path) in
                        
                        // download image
                        downloadImage(movie, completion: { (image) in
                            
                            // Transform and save to disk
                            transformAndSave(image, imageID: tmdbID, completion: { (image) in
                                
                                completion(movie: movie,
                                    image: image)
                            })
                        })
                    })
                }
            }
        }
    }
    
    private class func downloadImage(movie: Movie, completion:((image: UIImage?) -> Void)) {
        
        MediaAPIManager.retrieveTMDBImage(movie) { (image) in
            
            completion(image: image)
        }
    }
    
    private class func transformAndSave(image: UIImage?, imageID: String, completion:((image: UIImage?) -> Void)) {
        
        if let image = image {
            
            let operation = TransformAndSaveOperation(image: image,
                                                      imageID: imageID)
            
            operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
            
            operation.onCompletion = { (result: AnyObject?) -> Void in
                
                if let image = result as? UIImage {
                    
                    completion(image: image)
                }
                else {
                    
                    completion(image: nil)
                }
            }
            
            OperationQueueManager.sharedInstance.add(operation: operation)
        }
        else {
            
            completion(image: nil)
        }
    }
    
    private class func retrieveLocalImage(tmdbID: String, completion:((image: UIImage?) -> Void)) {
        
        let absolutePathImage = imageAbsolutePath(imageName: tmdbID)

        NSFileManager.fileExistsAtPath(absolutePathImage) { (fileExists: Bool) -> Void in
            
            // If project image cached on disk, retrieve and return
            if fileExists == true {
            
                let operation = RetrieveLocalImageOperation(imageName: tmdbID)
                
                operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
                
                operation.onCompletion = { (result: AnyObject?) -> Void in
                    
                    if let image = result as? UIImage {
                        
                        completion(image: image)
                    }
                    else {
                        
                        completion(image: nil)
                    }
                }
                
                OperationQueueManager.sharedInstance.add(operation: operation)
            }
            else {
                
                completion(image: nil)
            }
        }
    }
    
    private class func retrieveTMDBPath(movie: Movie, completion:((path: String?) -> Void)) {
        
        MoviesAPIManager.retrieveTMDBMovie(movie, success: { (result) in
            
            if let path = result as? String {
                
                completion(path: path)
            }
            else {
                
                completion(path: nil)
            }
        }) { (error) in
            
            completion(path: nil)
        }
    }
    
    //MARK: - AbsolutePath
    
    private class func imageAbsolutePath(imageName imageName: String) -> String {
        
        let cacheDirectory: String = NSFileManager.cacheDirectoryPath()
        let absolutePath: String = cacheDirectory.stringByAppendingString("/\(imageName)")
        
        return absolutePath
    }
}
