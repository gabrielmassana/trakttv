//
//  TransformAndSaveOperation.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class TransformAndSaveOperation: Operation {

    //MARK: - Accessors
    
    var image: UIImage
    var imageID: String
    
    //MARK: - Init
    
    init(image: UIImage, imageID: String) {
        
        self.image = image
        self.imageID = imageID
        
        super.init()
        
        identifier = "TransformAndSaveOperation - imageID:\(self.imageID)"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
        
        // Transform the image to the needed size
        let transformedImage = MediaTransformManager.tranformedMedia(image)
        
        // Save transformedImage
        transformedImage.saveToDisk(imageID)
        
        self.didCompleteWithResult(transformedImage)
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didCompleteWithResult(image)
    }
}
