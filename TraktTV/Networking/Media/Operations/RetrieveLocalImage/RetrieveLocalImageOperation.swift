//
//  RetrieveLocalImageOperation.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class RetrieveLocalImageOperation: Operation {
    
    //MARK: - Accessors
    
    var imageName: String
    
    //MARK: - Init
    
    init(imageName: String) {
        
        self.imageName = imageName
        
        super.init()
        
        identifier = "RetrieveLocalImageOperation - imageName: \(self.imageName)"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
            
        guard let data = NSFileManager.retrieveDataFromCacheDirectory(imageName),
            let image = UIImage(data: data) else {
                
                didCompleteWithResult(nil)
                return
        }
        
        didCompleteWithResult(image)
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didFailWithError(nil)
    }
}
