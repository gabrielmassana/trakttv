//
//  MoviesParseOperation.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreDataServices

class MoviesParseOperation: Operation {
    
    //MARK: - Accessors
    
    /// The response from API to be parsed.
    var responseObject: AnyObject
    
    /// The API response headers.
    var headers: [String : AnyObject]

    /// The parsing page.
    var page: Int

    /// Search criteria to be used as FeedID
    var searchCriteria: String
    
    //MARK: - Init
    
    init(responseObject: AnyObject, headers: [String : AnyObject], page: Int, searchCriteria: String) {
        
        self.responseObject = responseObject
        self.headers = headers
        self.page = page
        self.searchCriteria = searchCriteria
        
        super.init()
        
        identifier = "MoviesParseOperation - page:\(page) - searchCriteria:\(searchCriteria)"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
        
        ServiceManager.sharedInstance.backgroundManagedObjectContext.performBlockAndWait {
            
            guard let response = self.responseObject as? [[String : AnyObject]] else {
                
                self.didCompleteWithResult(nil)
                
                return
            }
            
            let feedParser = MoviesFeedParser.parser(ServiceManager.sharedInstance.backgroundManagedObjectContext)

            let feed = feedParser.parseFeed(self.searchCriteria, serverResponse: response, headers: self.headers)
           
            ServiceManager.sharedInstance.saveBackgroundManagedObjectContext()
            
            self.didCompleteWithResult(feed?.feedID)
        }
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didCompleteWithResult(nil)
    }
}
