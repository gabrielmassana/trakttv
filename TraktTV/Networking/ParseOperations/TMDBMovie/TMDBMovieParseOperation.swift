//
//  TMDBMovieParseOperation.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreDataServices

class TMDBMovieParseOperation: Operation {

    //MARK: - Accessors
    
    /// The response from API to be parsed.
    var responseObject: AnyObject
    
    /// The movie ID: traktID.
    var traktID: String
    
    //MARK: - Init
    
    init(responseObject: AnyObject, traktID: String) {
        
        self.responseObject = responseObject
        self.traktID = traktID
        
        super.init()
        
        identifier = "TMDBMovieParseOperation - traktID:\(traktID)"
    }
    
    //MARK: - Start
    
    override func start() {
        
        super.start()
        
        ServiceManager.sharedInstance.backgroundManagedObjectContext.performBlockAndWait {
                        
            guard let response = self.responseObject as? [String : AnyObject] else {
                
                self.didCompleteWithResult(nil)
                
                return
            }
            
            let movieParser = TMDBMovieParser.parser(ServiceManager.sharedInstance.backgroundManagedObjectContext)

            let movie = movieParser.parseMovie(response,
                traktID: self.traktID)
            
            ServiceManager.sharedInstance.saveBackgroundManagedObjectContext()

            if let movie = movie {
                self.didCompleteWithResult(movie.tmdbImagePath)
            }
            else {
                
                self.didCompleteWithResult(nil)
            }
        }
    }
    
    //MARK: - Cancel
    
    override func cancel() {
        
        super.cancel()
        
        didCompleteWithResult(nil)
    }
}
