//
//  MediaRequest.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import Alamofire

class MediaRequest: Request {

    //MARK: - Image

    static func requestToRetrieveMoviesImage(tmdbID: String, tmdbImagePath: String) -> NSMutableURLRequest {
        
        let request = requestTMDB()
        
        request.tmdbImageEndpoint(tmdbImagePath)

        return request
    }
}
