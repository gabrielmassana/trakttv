//
//  Request.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

struct HTTPRequestMethod {
    
    static let Get: String = "GET"
    static let Post: String = "POST"
    static let Put: String = "PUT"
    static let Delete: String = "DELETE"
}

///  Abstract request instance.
class Request: NSMutableURLRequest {

    //MARK: - Init
    
    /// Instanciate a NSMutableURLRequest for TraktTV API.
    class func requestTraktTV() -> NSMutableURLRequest  {
        
        let mutableURLRequest = self.init()
        
        mutableURLRequest.addTraktTVCommonHeaders()
        
        return mutableURLRequest
    }
    
    /// Instanciate a NSMutableURLRequest for TraktTV API.
    class func requestTMDB() -> NSMutableURLRequest  {
        
        let mutableURLRequest = self.init()
        
        return mutableURLRequest
    }
}

extension NSMutableURLRequest {
    
    //MARK: - Endpoint

    /**
     Update the request API endpoint with the given endpoint string and the APIConfig data.
     
     - Parameter endpoint: the specific endpoint for the request.
     */
    func traktEndpoint(endpoint: String) {
        
        self.URL = NSURL(string: traktAPIPathWithEndpoint(endpoint))
    }
    
    func tmdbEndpoint(endpoint: String) {
        
        self.URL = NSURL(string: tmdbAPIPathWithEndpoint(endpoint))
    }
    
    func tmdbImageEndpoint(endpoint: String) {
        
        self.URL = NSURL(string: tmdbImagePathWithEndpoint(endpoint))
    }
    
    /**
     Attaches to the APIConfig data the given endpoint and returns a new String with the whole URL.
     
     - Parameter endpoint: the specific endpoint for the request.
     */
    private func traktAPIPathWithEndpoint(endpoint: String) -> String {
        
        let config = APIConfig.sharedInstance
        
        let path = String(format: "%@%@", config.TraktAPIHost, endpoint)

        return path
    }
    
    private func tmdbAPIPathWithEndpoint(endpoint: String) -> String {
        
        let config = APIConfig.sharedInstance
        
        let path = String(format: "%@/%@%@", config.TMDBAPIHost, config.TMDBAPIVersion, endpoint)
        
        return path
    }
    
    private func tmdbImagePathWithEndpoint(endpoint: String) -> String {
        
        let config = APIConfig.sharedInstance
        
        let path = String(format: "%@/%@%@", config.TMDBImageHost, config.TMDBFileSize, endpoint)
        
        return path
    }
    
    //MARK: - HTTPHeaders

    /// Updated the Request with common headers.
    func addTraktTVCommonHeaders() {
        
        self.addValue("application/json",
                      forHTTPHeaderField: "Content-type")
        
        self.addValue(APIConfig.sharedInstance.TraktAPIClientID,
                      forHTTPHeaderField: "trakt-api-key")
        
        self.addValue(APIConfig.sharedInstance.TraktAPIVersion,
                      forHTTPHeaderField: "trakt-api-version")
    }
}
