//
//  MoviesRequest.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import Alamofire

class MoviesRequest: Request {

    //MARK: - Movies
    
    /// Request to retreive first movies page.
    static func requestToRetrieveMovies(searchCriteria: String, page: Int) -> NSMutableURLRequest {
        
        var request = requestTraktTV()
        
        request.HTTPMethod = HTTPRequestMethod.Get
        
        var parameters: [String : AnyObject] = ["page" : page]
        
        if searchCriteria != MoviesFeedID {
            
            parameters["query"] = searchCriteria
        }
        
        request.traktEndpoint("/movies/popular")
        
        let encoding = Alamofire.ParameterEncoding.URL
        (request, _) = encoding.encode(request , parameters: parameters)
        
        return request
    }
    
    /// Request to retreive a TMDB movie data.
    static func requestToRetrieveTMDBMovie(tmdbID: String) -> NSMutableURLRequest {
        
        var request = requestTMDB()
        
        request.HTTPMethod = HTTPRequestMethod.Get
        
        let parameters: [String : AnyObject] = ["api_key" : APIConfig.sharedInstance.TMDBAPIKey]
        
        request.tmdbEndpoint("/movie/\(tmdbID)")
        
        let encoding = Alamofire.ParameterEncoding.URL
        (request, _) = encoding.encode(request , parameters: parameters)
        
        return request
    }
}
