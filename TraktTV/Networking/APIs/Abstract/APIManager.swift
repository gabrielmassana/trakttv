//
//  APIManager.swift
//  TraktTV
//
//  Created by GabrielMassana on 19/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

typealias NetworkingOnSuccess = (result: AnyObject?) -> Void
typealias NetworkingOnFailure = (error: NSError?) -> Void
typealias NetworkingOnCompletion = (result: AnyObject?) -> Void

///  Abstract API Manager instance with callback accessors.
class APIManager: NSObject {
    
    //MARK: - Accessors
    
    /// Callback called when the networking operation completes succesfully.
    var onSuccess: NetworkingOnSuccess?
    
    /// Callback called when the networking operation fails.
    var onFailure: NetworkingOnFailure?
    
    /// Callback called when the networking operation completes.
    var onCompletion: NetworkingOnCompletion?
}
