//
//  MediaAPIManager.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class MediaAPIManager: APIManager {

    class func retrieveTMDBImage(movie: Movie, completion:((image: UIImage?) -> Void)) {
        
        guard let tmdbID = movie.tmdbID,
            tmdbImagePath = movie.tmdbImagePath else {
                
                completion(image: nil)
                return
        }
        
        let request = MediaRequest.requestToRetrieveMoviesImage(tmdbID,
                                                                tmdbImagePath: tmdbImagePath)

        NetworkingManager.defaultManager.request(request)
            .responseData { (response) -> Void in
                
                guard let data = response.data,
                    let image = UIImage(data: data) else {
                        
                        completion(image: nil)
                        return
                }
                
                completion(image: image)
        }
    }
}
