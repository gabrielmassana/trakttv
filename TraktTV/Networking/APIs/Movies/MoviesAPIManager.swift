//
//  MoviesAPIManager.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class MoviesAPIManager: APIManager {
    
    //MARK: - Movies
    
    class func retrieveMoviesWith(searchCriteria: String, page: Int, success: NetworkingOnSuccess, failure: NetworkingOnFailure) {
        
        let request = MoviesRequest.requestToRetrieveMovies(searchCriteria,
                                                            page: page)
        
        NetworkingManager.defaultManager.request(request)
            .validate()
            .responseJSONTraktTV { (response, error) in
                
                switch response.result {
                    
                case .Success:
                    
                    if let responseObject = response.result.value,
                        let httpresponse = response.response,
                        let headers = httpresponse.allHeaderFields as? [String : AnyObject] {
                        
                        let operation = MoviesParseOperation(responseObject: responseObject,
                            headers: headers,
                            page: page,
                            searchCriteria: searchCriteria)
                        
                        operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
                        
                        operation.onCompletion = { (result: AnyObject?) -> Void in
                            
                            success(result: result)
                        }
                        
                        OperationQueueManager.sharedInstance.add(operation: operation)
                    }
                    else {
                        
                        failure(error: error)
                    }
                    
                case .Failure(_):
                    
                    failure(error: error)
                }
        }
    }
    
    class func retrieveTMDBMovie(movie: Movie, success: NetworkingOnSuccess, failure: NetworkingOnFailure) {
        
        guard let tmdbID = movie.tmdbID,
            traktID = movie.traktID else {
                
                failure(error: nil)
                return
        }
        
        let request = MoviesRequest.requestToRetrieveTMDBMovie(tmdbID)
        
        NetworkingManager.defaultManager.request(request)
            .validate()
            .responseJSONTraktTV { (response, error) in
                
                switch response.result {
                    
                case .Success:
                    
                    if let responseObject = response.result.value {
                        
                        let operation = TMDBMovieParseOperation(responseObject: responseObject,
                            traktID: traktID)
                        
                        operation.operationQueueIdentifier = LocalDataOperationQueueTypeIdentifier
                        
                        operation.onCompletion = { (result: AnyObject?) -> Void in
                            
                            success(result: result)
                        }
                        
                        OperationQueueManager.sharedInstance.add(operation: operation)
                    }
                    else {
                        
                        failure(error: nil)
                    }
                    
                case .Failure(_):
                    
                    failure(error: error)
                }
        }
    }
}
