//
//  TMDBMovieSerializer.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

struct TMDBMovieSerializer {

    var posterPath: String
    
    init?(serverResponse: [String : AnyObject]) {
        
        guard let posterPath = serverResponse["poster_path"] as? String else {
                
                return nil
        }
        
        self.posterPath = posterPath
    }
}
