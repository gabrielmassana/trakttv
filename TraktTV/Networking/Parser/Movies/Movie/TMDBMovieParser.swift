//
//  TMDBMovieParser.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class TMDBMovieParser: Parser {

    func parseMovie(serverResponse: [String : AnyObject], traktID: String) -> Movie? {

        // Serializer
        guard let movieSerializer = TMDBMovieSerializer(serverResponse: serverResponse) else {
            
            return nil
        }
        
        let movie = Movie.fetchMovie(traktID,
                                     managedObjectContext: parserManagedObjectContext!)
        
        movie?.tmdbImagePath = movieSerializer.posterPath
        
        return movie
    }
}
