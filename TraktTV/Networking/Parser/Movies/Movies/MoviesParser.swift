//
//  MoviesParser.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

import CoreData
import CoreDataServices

class MoviesParser: Parser {

    func parseMovies(serverResponse: [[String : AnyObject]]) -> [Movie] {
        
        var movies = [Movie]()
        
        for serverMovie in serverResponse {
            
            let movie = parseMovie(serverMovie)
            
            if let movie = movie {
                
                movies.append(movie)
            }
        }
        
        return movies
    }
    
    private func parseMovie(serverResponse: [String : AnyObject]) -> Movie? {
        
        // Serializer
        guard let movieSerializer = MovieSerializer(serverResponse: serverResponse) else {
            
            return nil
        }
        
        // Fetch Movie
        var movie = Movie.fetchMovie(movieSerializer.traktID, managedObjectContext: parserManagedObjectContext!)
        
        if movie == nil {
            
            movie = NSEntityDescription.insertNewObjectForEntity(Movie.self,
                                                                 managedObjectContext: parserManagedObjectContext!) as? Movie
            
            movie?.traktID = movieSerializer.traktID
            movie?.creationDate = NSDate()
        }
        
        // Parse values
        movie?.title = movieSerializer.title
        movie?.year = movieSerializer.year
        movie?.tmdbID = movieSerializer.tmdbID
        
        return movie
    }
}
