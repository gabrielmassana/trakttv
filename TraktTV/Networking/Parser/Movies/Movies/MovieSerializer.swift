//
//  MovieSerializer.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

struct MovieSerializer {
    
     var traktID: String
    
     var title: String?
     var year: String?
     var tmdbID: String?
    
    init?(serverResponse: [String : AnyObject]) {
        
        guard let ids = serverResponse["ids"] as? [String : AnyObject],
            let traktID = ids["trakt"] as? NSNumber else {
                
                return nil
        }
        
        self.traktID = traktID.stringValue

        if let tmdbID = ids["tmdb"] as? NSNumber {
            
            self.tmdbID = tmdbID.stringValue
        }
        
        if let title = serverResponse["title"] as? String {
            
            self.title = title
        }
        
        if let year = serverResponse["year"] as? NSNumber {
            
            self.year = year.stringValue
        }
    }
}