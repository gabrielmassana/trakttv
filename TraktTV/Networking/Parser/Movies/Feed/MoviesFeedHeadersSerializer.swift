//
//  MoviesFeedHeadersSerializer.swift
//  TraktTV
//
//  Created by GabrielMassana on 22/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import Foundation

struct MoviesFeedHeadersSerializer {
    
    var currentPage: NSNumber?
    var totalPages: NSNumber?
    var totalItems: NSNumber?
    
    init?(headers: [String : AnyObject]) {
        
        if let currentPage = headers["x-pagination-page"] as? String {
            
            self.currentPage = Int(currentPage)
        }
        
        if let totalPages = headers["x-pagination-page-count"] as? String {
            
            self.totalPages = Int(totalPages)
        }
        
        if let totalItems = headers["x-pagination-item-count"] as? String {
            
            self.totalItems = Int(totalItems)
        }
    }
}
