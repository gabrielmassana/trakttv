//
//  MoviesFeedParser.swift
//  TraktTV
//
//  Created by GabrielMassana on 21/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import UIKit

class MoviesFeedParser: Parser {

    func parseFeed(feedID: String, serverResponse: [[String : AnyObject]], headers: [String : AnyObject]) -> MoviesFeed? {
        
        // Fetch Feed
        let feed = MoviesFeed.fetchOrInsertMoviesFeed(feedID,
                                                      managedObjectContext: parserManagedObjectContext!)

        // Parse movies
        let moviesParser = MoviesParser.parser(parserManagedObjectContext!)
        
        let parsedMovies = moviesParser.parseMovies(serverResponse)
        
        for movie in parsedMovies {
            
            feed?.addMoviesObject(movie)
        }

        // Serializer
        if let moviesFeedHeadersSerializer = MoviesFeedHeadersSerializer(headers: headers) {
            
            feed?.currentPage = moviesFeedHeadersSerializer.currentPage
            feed?.totalPages = moviesFeedHeadersSerializer.totalPages
            feed?.totalItems = moviesFeedHeadersSerializer.totalItems
        }
        
        return feed
    }
}
