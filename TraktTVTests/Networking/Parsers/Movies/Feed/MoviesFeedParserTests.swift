//
//  MoviesFeedParserTests.swift
//  TraktTV
//
//  Created by GabrielMassana on 23/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import TraktTV

import CoreDataServices

class MoviesFeedParserTests: XCTestCase {

    //MARK: - Accessors

    var parser: MoviesFeedParser?
    
    var moviesJSON: [[String : AnyObject]]?
    var movieJSON: [String : AnyObject]?
    var idsJSON: [String : AnyObject]?
    var headers: [String : AnyObject]?
    
    var tmdb: NSNumber?
    var trakt: NSNumber?
    var title: String?
    var year: NSNumber?
    
    var page: String?
    var totalPageCount: String?
    var totalItemCount: String?
    
    var feedID: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = MoviesFeedParser.parser(ServiceManager.sharedInstance.backgroundManagedObjectContext)
        
        tmdb = 157336
        trakt = 102156
        title = "Interstellar"
        year = 2014
        
        feedID = "-1"
        
        idsJSON = [
            
            "tmdb" : tmdb!,
            "trakt" : trakt!
        ]
        
        movieJSON = [
            
            "ids" : idsJSON!,
            "title" : title!,
            "year" : year!

        ]
        
        moviesJSON = [
            
            movieJSON!,
            [
                "ids" : [
                    
                    "tmdb" : 49026,
                    "trakt" : 34073
                ],
                "title" : "The Dark Knight Rises",
                "year" : 2012
                
            ],
            [
                "ids" : [
                    
                    "tmdb" : 19995,
                    "trakt" : 12269
                ],
                "title" : "Avatar",
                "year" : 2009
                
            ]
        ]
        
        page = "1"
        totalPageCount = "20"
        totalItemCount = "200"
        
        headers = [
            
            "x-pagination-page" : page!,
            "x-pagination-page-count" : totalPageCount!,
            "x-pagination-item-count" : totalItemCount!
        ]
    }
    
    override func tearDown() {
        
        ServiceManager.sharedInstance.clear()
        
        parser = nil
        
        moviesJSON = nil
        movieJSON = nil
        idsJSON = nil
        headers = nil
        
        tmdb = nil
        trakt = nil
        title = nil
        year = nil
        
        page = nil
        totalPageCount = nil
        totalItemCount = nil
        
        feedID = nil
        
        super.tearDown()
    }
    
    // MARK: - Feed
    
    func test_parseFeed_newFeedObjectReturned() {
        
        let feed = parser?.parseFeed(feedID!,
                                     serverResponse: moviesJSON!,
                                     headers: headers!)
        
        XCTAssertNotNil(feed, "A valid Feed object wasn't created");
    }
    
    func test_parseFeed_feedID() {
        
        let feed = parser?.parseFeed(MoviesFeedID,
                                     serverResponse: moviesJSON!,
                                     headers: headers!)
        
        XCTAssertTrue(feed!.feedID == feedID!, "feedID was set wrongly");
    }
    
    func test_parseFeed_currentPage() {
        
        let feed = parser?.parseFeed(MoviesFeedID,
                                     serverResponse: moviesJSON!,
                                     headers: headers!)
        
        XCTAssertTrue(feed!.currentPage == Int(page!), "currentPage was set wrongly");
    }
    
    func test_parseFeed_totalPages() {
        
        let feed = parser?.parseFeed(MoviesFeedID,
                                     serverResponse: moviesJSON!,
                                     headers: headers!)
        
        XCTAssertTrue(feed!.totalPages == Int(totalPageCount!), "totalPages was set wrongly");
    }
    
    func test_parseFeed_totalItems() {
        
        let feed = parser?.parseFeed(MoviesFeedID,
                                     serverResponse: moviesJSON!,
                                     headers: headers!)
        
        XCTAssertTrue(feed!.totalItems == Int(totalItemCount!), "totalItems was set wrongly");
    }
}
