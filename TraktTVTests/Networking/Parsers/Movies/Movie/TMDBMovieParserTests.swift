//
//  TMDBMovieParserTests.swift
//  TraktTV
//
//  Created by GabrielMassana on 23/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import TraktTV

import CoreDataServices

class TMDBMovieParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: TMDBMovieParser?
    
    var movieJSON: [String : AnyObject]?
    
    var posterPath: String?
    
    var trakt: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = TMDBMovieParser.parser(ServiceManager.sharedInstance.backgroundManagedObjectContext)
        
        posterPath = "1w23e423r465t743y7.jpg"
        trakt = "102156"
        
        movieJSON = [
            
            "poster_path" : posterPath!,
        ]
    }
    
    override func tearDown() {
        
        ServiceManager.sharedInstance.clear()
        
        parser = nil
        
        movieJSON = nil
        
        posterPath = nil
        trakt = nil
        
        super.tearDown()
    }
    
    // MARK: - TMDBMovieParser
    
    func test_moviesParser_movieObjectReturned() {
        
        let movie = parser?.parseMovie(movieJSON!,
                                        traktID: trakt!)
        
        XCTAssertNotNil(movie, "A valid Movie object wasn't created");
    }
    
    func test_moviesParser_posterPath() {
        
        let movie = parser?.parseMovie(movieJSON!,
                                       traktID: trakt!)
        
        XCTAssertEqual(movie!.tmdbImagePath, posterPath!, String(format:"Parsed object is wrong: %@", movie!));
    }
}
