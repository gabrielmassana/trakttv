//
//  MoviesParserTests.swift
//  TraktTV
//
//  Created by GabrielMassana on 23/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import TraktTV

import CoreDataServices

class MoviesParserTests: XCTestCase {
    
    //MARK: - Accessors
    
    var parser: MoviesParser?
    
    var moviesJSON: [[String : AnyObject]]?
    var movieJSON: [String : AnyObject]?
    var idsJSON: [String : AnyObject]?
    
    var tmdb: NSNumber?
    var trakt: NSNumber?
    var title: String?
    var year: NSNumber?
    
    var page: String?
    var totalPageCount: String?
    var totalItemCount: String?
    
    var feedID: String?
    
    // MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        parser = MoviesParser.parser(ServiceManager.sharedInstance.backgroundManagedObjectContext)
        
        tmdb = 157336
        trakt = 102156
        title = "Interstellar"
        year = 2014
        
        feedID = "-1"
        
        idsJSON = [
            
            "tmdb" : tmdb!,
            "trakt" : trakt!
        ]
        
        movieJSON = [
            
            "ids" : idsJSON!,
            "title" : title!,
            "year" : year!
            
        ]
        
        moviesJSON = [
            
            movieJSON!,
            [
                "ids" : [
                    
                    "tmdb" : 49026,
                    "trakt" : 34073
                ],
                "title" : "The Dark Knight Rises",
                "year" : 2012
                
            ],
            [
                "ids" : [
                    
                    "tmdb" : 19995,
                    "trakt" : 12269
                ],
                "title" : "Avatar",
                "year" : 2009
                
            ]
        ]
        
        page = "1"
        totalPageCount = "20"
        totalItemCount = "200"
    }
    
    override func tearDown() {
        
        ServiceManager.sharedInstance.clear()
        
        parser = nil
        
        moviesJSON = nil
        movieJSON = nil
        idsJSON = nil
        
        tmdb = nil
        trakt = nil
        title = nil
        year = nil
        
        page = nil
        totalPageCount = nil
        totalItemCount = nil
        
        feedID = nil
        
        super.tearDown()
    }
    
    // MARK: - MoviesParser
    
    func test_moviesParser_newMovieObjectReturned() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        XCTAssertNotNil(movies, "A valid Movies Array object wasn't created");
    }
    
    func test_moviesParser_count() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let arrayCount = NSNumber(integer: movies!.count)
        let jsonCount = NSNumber(integer: moviesJSON!.count)
        
        XCTAssertTrue(arrayCount == jsonCount, String(format:"Parsed count is wrong. Should be %@ rather than: %@", arrayCount, jsonCount));
    }
    
    func test_parseMovies_uniqueObjects() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let firstObject = movies!.first!
        let lastObject = movies!.last!
        
        XCTAssertNotEqual(firstObject, lastObject, String(format:"Parsed objects are wrong: %@ and %@", firstObject, lastObject));
    }
    
    func test_parseMovies_firstObject_traktID() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let firstObject = movies!.first!
        
        XCTAssertEqual(firstObject.traktID, trakt!.stringValue, String(format:"Parsed object is wrong: %@", firstObject));
    }
    
    func test_parseMovies_firstObject_tmdbID() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let firstObject = movies!.first!
        
        XCTAssertEqual(firstObject.tmdbID, tmdb!.stringValue, String(format:"Parsed object is wrong: %@", firstObject));
    }
    
    func test_parseMovies_firstObject_title() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let firstObject = movies!.first!
        
        XCTAssertEqual(firstObject.title, title!, String(format:"Parsed object is wrong: %@", firstObject));
    }
    
    func test_parseMovies_firstObject_year() {
        
        let movies = parser?.parseMovies(moviesJSON!)
        
        let firstObject = movies!.first!
        
        XCTAssertEqual(firstObject.year, year!.stringValue, String(format:"Parsed object is wrong: %@", firstObject));
    }
}
