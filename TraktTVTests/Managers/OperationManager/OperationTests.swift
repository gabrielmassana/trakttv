//
//  OperationTests.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import TraktTV

class OperationTests: XCTestCase {
    
    //MARK: - Accessors
    
    var firstOperation: Operation?
    var secondOperation: Operation?
    
    var firstOperationIdentifier: String?
    var secondOperationIdentifier: String?
    
    var onSuccess: OperationOnSuccess?
    var onFailure: OperationOnFailure?
    var onCompletion: OperationOnCompletion?
    
    //MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        firstOperation = Operation()
        secondOperation = Operation()
        
        firstOperationIdentifier = "firstOperationIdentifier"
        secondOperationIdentifier = "secondOperationIdentifier"
        
        onSuccess = { (result: AnyObject?) -> Void in
            
            print(result)
        }
        
        onFailure = { (error: NSError?) -> Void in
            
            print(error)
        }
        
        onCompletion = { (result: AnyObject?) -> Void in
            
            print(result)
        }
    }
    
    override func tearDown() {
        
        firstOperation = nil
        secondOperation = nil
        
        firstOperationIdentifier = nil
        secondOperationIdentifier = nil
        
        onSuccess = nil
        onFailure = nil
        onCompletion = nil
        
        super.tearDown()
    }
    
    //MARK: - Operation
    
    func test_operationQueueManager_identifier() {
        
        firstOperation!.identifier = firstOperationIdentifier
        
        XCTAssertEqual(firstOperation!.identifier, firstOperationIdentifier!, String(format:"identifier was not set properly. Was set to: %@ rather than: %@", firstOperation!.identifier!, firstOperationIdentifier!))
    }
    
    func test_operationQueueManager_initReady() {
        
        XCTAssertTrue(firstOperation!.ready, "Operation was not init properly. Must be Ready to execute.")
    }
    
    func test_operationQueueManager_initExecuting() {
        
        XCTAssertFalse(firstOperation!.executing, "Operation was not init properly. Must be Not Executing after init.")
    }
    
    func test_operationQueueManager_initFinished() {
        
        XCTAssertFalse(firstOperation!.finished, "Operation was not init properly. Must be Not Finished after init.")
    }
    
    func test_operationQueueManager_startReady() {
        
        firstOperation!.start()
        
        XCTAssertFalse(firstOperation!.ready, "Operation was not init properly. Must be NOT Ready after start.")
    }
    
    func test_operationQueueManager_startExecuting() {
        
        firstOperation!.start()
        
        XCTAssertTrue(firstOperation!.executing, "Operation was not init properly. Must be Executing after start.")
    }
    
    func test_operationQueueManager_startFinished() {
        
        firstOperation!.start()
        
        XCTAssertFalse(firstOperation!.finished, "Operation was not init properly. Must be Not Finished after start.")
    }
    
    func test_operationQueueManager_finishReady() {
        
        firstOperation!.start()
        firstOperation!.finish()
        
        XCTAssertFalse(firstOperation!.ready, "Operation was not init properly. Must be NOT Ready after finish.")
    }
    
    func test_operationQueueManager_finishExecuting() {
        
        firstOperation!.start()
        firstOperation!.finish()
        
        XCTAssertFalse(firstOperation!.executing, "Operation was not init properly. Must be NOT Executing after finish.")
    }
    
    func test_operationQueueManager_finishFinished() {
        
        firstOperation!.start()
        firstOperation!.finish()
        
        XCTAssertTrue(firstOperation!.finished, "Operation was not init properly. Must be Finished after finish.")
    }
    
    func test_operationQueueManager_canCoalesceWithOperationDiferentIdentifier() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = secondOperationIdentifier
        
        let canCoalesce = firstOperation!.canCoalesceWithOperation(secondOperation!)
        
        XCTAssertFalse(canCoalesce, "Operation with different identifiers can not coalesce")
    }
    
    func test_operationQueueManager_canCoalesceWithOperationSameIdentifier() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        let canCoalesce = firstOperation!.canCoalesceWithOperation(secondOperation!)
        
        XCTAssertTrue(canCoalesce, "Operation with same identifiers can coalesce")
    }
    
    func test_operationQueueManager_onSuccessNew() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onSuccess = onSuccess
        let onSuccessBefore = unsafeBitCast(onSuccess, AnyObject.self)
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        let onSuccessAfter = unsafeBitCast(firstOperation!.onSuccess, AnyObject.self)
        
        XCTAssertFalse(onSuccessBefore === onSuccessAfter, "onSuccess must be a new one after coalesce.")
    }
    
    func test_operationQueueManager_onSuccessNotNil() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onSuccess = onSuccess
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        
        XCTAssertNotNil(firstOperation!.onSuccess, "onSuccess must be not nil after coalesce.")
    }
    
    func test_operationQueueManager_onFailureNew() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onFailure = onFailure
        let onFailureBefore = unsafeBitCast(onFailure, AnyObject.self)
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        let onFailureAfter = unsafeBitCast(firstOperation!.onFailure, AnyObject.self)
        
        XCTAssertFalse(onFailureBefore === onFailureAfter, "onFailure must be a new one after coalesce.")
    }
    
    func test_operationQueueManager_onFailureNotNil() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onFailure = onFailure
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        
        XCTAssertNotNil(firstOperation!.onFailure, "onFailure must be not nil after coalesce.")
    }
    
    func test_operationQueueManager_onCompletionNew() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onCompletion = onCompletion
        let onCompletionBefore = unsafeBitCast(onCompletion, AnyObject.self)
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        let onCompletionAfter = unsafeBitCast(firstOperation!.onCompletion, AnyObject.self)
        
        XCTAssertFalse(onCompletionBefore === onCompletionAfter, "onCompletion must be a new one after coalesce.")
    }
    
    func test_operationQueueManager_onCompletionNotNil() {
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.onCompletion = onCompletion
        
        firstOperation!.coalesceWithOperation(secondOperation!)
        
        XCTAssertNotNil(firstOperation!.onCompletion, "onCompletion must be not nil after coalesce.")
    }
}
