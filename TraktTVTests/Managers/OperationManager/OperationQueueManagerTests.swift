//
//  OperationQueueManagerTests.swift
//  TraktTV
//
//  Created by GabrielMassana on 20/10/2016.
//  Copyright © 2016 GabrielMassana. All rights reserved.
//

import XCTest

@testable import TraktTV

class OperationQueueManagerTests: XCTestCase {
    
    //MARK: - Accessors
    
    var operationQueueManager: OperationQueueManager?
    
    var defaultOperationQueue: NSOperationQueue?
    var updatedOperationQueue: NSOperationQueue?
    
    var defaultOperationQueueTypeIdentifier: String?
    var updatedOperationQueueTypeIdentifier: String?
    
    var maxConcurrentOperationCount: Int?
    var qualityOfService: NSQualityOfService?
    
    var firstOperation: Operation?
    var secondOperation: Operation?
    
    var firstOperationIdentifier: String?
    var secondOperationIdentifier: String?
    
    //MARK: - TestSuiteLifecycle
    
    override func setUp() {
        
        super.setUp()
        
        operationQueueManager = OperationQueueManager()
        
        defaultOperationQueue = NSOperationQueue()
        updatedOperationQueue = NSOperationQueue()
        
        defaultOperationQueueTypeIdentifier = "defaultOperationQueueTypeIdentifier"
        updatedOperationQueueTypeIdentifier = "updatedOperationQueueTypeIdentifier"
        
        maxConcurrentOperationCount = 5
        qualityOfService = .Default
        
        firstOperation = Operation()
        secondOperation = Operation()
        
        firstOperationIdentifier = "firstOperationIdentifier"
        secondOperationIdentifier = "secondOperationIdentifier"
    }
    
    override func tearDown() {
        
        operationQueueManager = nil
        
        defaultOperationQueue = nil
        updatedOperationQueue = nil
        
        defaultOperationQueueTypeIdentifier = nil
        updatedOperationQueueTypeIdentifier = nil
        
        maxConcurrentOperationCount = nil
        qualityOfService = nil
        
        firstOperation = nil
        secondOperation = nil
        
        firstOperationIdentifier = nil
        secondOperationIdentifier = nil
        
        super.tearDown()
    }
    
    //MARK: - OperationQueueManager
    
    func test_operationQueueManager_add() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        XCTAssertNotNil(operationQueueManager!.operationQueuesDictionary[defaultOperationQueueTypeIdentifier!], "A valid OperationQueue object wasn't added to OperationQueueManager")
    }
    
    func test_operationQueueManager_retrieveSameQueue() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        let addedQueue = operationQueueManager!.operationQueuesDictionary[defaultOperationQueueTypeIdentifier!]
        
        XCTAssertTrue(addedQueue! == defaultOperationQueue, String(format:"OperationQueue was not set properly. Was set to: %@ rather than: %@", addedQueue!, defaultOperationQueue!))
    }
    
    func test_operationQueueManager_count() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        operationQueueManager!.register(operationQueue: updatedOperationQueue!, operationQueueIdentifier: updatedOperationQueueTypeIdentifier!)
        
        XCTAssertTrue(operationQueueManager!.operationQueuesDictionary.count == 2, String(format:"OperationQueue count is not correct. OperationQueueManager has: %@ rather than: %@", operationQueueManager!.operationQueuesDictionary.count, 2))
    }
    
    func test_operationQueueManager_differentObjects() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        operationQueueManager!.register(operationQueue: updatedOperationQueue!, operationQueueIdentifier: updatedOperationQueueTypeIdentifier!)
        
        XCTAssertNotEqual(operationQueueManager!.operationQueuesDictionary[defaultOperationQueueTypeIdentifier!], operationQueueManager!.operationQueuesDictionary[updatedOperationQueueTypeIdentifier!], "Stored OperationQueues are not correct.")
    }
    
    func test_operationQueueManager_addedTwice() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        XCTAssertTrue(operationQueueManager!.operationQueuesDictionary.count == 1, String(format:"OperationQueue count is not correct. OperationQueueManager has: %@ rather than: %@", operationQueueManager!.operationQueuesDictionary.count, 1))
    }
    
    func test_operationQueueManager_qualityOfServiceDefault() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        XCTAssertTrue(defaultOperationQueue!.qualityOfService == NSQualityOfService.Default, String(format:"qualityOfService is not correct. Was set to: %@ rather than: %@", defaultOperationQueue!.qualityOfService.rawValue, NSQualityOfService.Default.rawValue))
    }
    
    func test_operationQueueManager_qualityOfServiceUpdated() {
        
        operationQueueManager!.register(operationQueue: updatedOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        updatedOperationQueue!.qualityOfService = qualityOfService!
        
        XCTAssertTrue(updatedOperationQueue!.qualityOfService == qualityOfService!, String(format:"qualityOfService is not correct. Was set to: %@ rather than: %@", updatedOperationQueue!.qualityOfService.rawValue, qualityOfService!.rawValue))
    }
    
    func test_operationQueueManager_maxConcurrentOperationCountDefault() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        XCTAssertTrue(defaultOperationQueue!.maxConcurrentOperationCount == NSOperationQueueDefaultMaxConcurrentOperationCount, String(format:"qualityOfService is not correct. Was set to: %@ rather than: %@", defaultOperationQueue!.maxConcurrentOperationCount, NSOperationQueueDefaultMaxConcurrentOperationCount))
    }
    
    func test_operationQueueManager_maxConcurrentOperationCountUpdated() {
        
        operationQueueManager!.register(operationQueue: updatedOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        updatedOperationQueue!.maxConcurrentOperationCount = maxConcurrentOperationCount!
        
        XCTAssertTrue(updatedOperationQueue!.maxConcurrentOperationCount == maxConcurrentOperationCount!, String(format:"qualityOfService is not correct. Was set to: %@ rather than: %@", updatedOperationQueue!.qualityOfService.rawValue, maxConcurrentOperationCount!))
    }
    
    func test_operationQueueManager_addOperationSameIdentifier() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = firstOperationIdentifier
        
        firstOperation!.operationQueueIdentifier = defaultOperationQueueTypeIdentifier
        secondOperation!.operationQueueIdentifier = defaultOperationQueueTypeIdentifier
        
        operationQueueManager?.add(operation: firstOperation!)
        operationQueueManager?.add(operation: secondOperation!)
        
        XCTAssertTrue(defaultOperationQueue!.operations.count == 1, String(format:"OperationQueue count is not correct. defaultOperationQueue has: %@ rather than: %@", operationQueueManager!.operationQueuesDictionary.count, 1))
    }
    
    func test_operationQueueManager_addOperationDiferentIdentifier() {
        
        operationQueueManager!.register(operationQueue: defaultOperationQueue!, operationQueueIdentifier: defaultOperationQueueTypeIdentifier!)
        
        firstOperation!.identifier = firstOperationIdentifier
        secondOperation!.identifier = secondOperationIdentifier
        
        firstOperation!.operationQueueIdentifier = defaultOperationQueueTypeIdentifier
        secondOperation!.operationQueueIdentifier = defaultOperationQueueTypeIdentifier
        
        operationQueueManager?.add(operation: firstOperation!)
        operationQueueManager?.add(operation: secondOperation!)
        
        XCTAssertTrue(defaultOperationQueue!.operations.count == 2, String(format:"OperationQueue count is not correct. defaultOperationQueue has: %@ rather than: %@", operationQueueManager!.operationQueuesDictionary.count, 2))
    }
}
